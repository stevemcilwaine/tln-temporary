import {ExampleComponent} from './ExampleComponent.js';

// Note: !!! Use this only in non WP builds to support the loading of SVG spritemaps in IE11.
//       Please update the url to point at your local spritemap.svg.
//       For WP builds, this code should be in PHP, where the dynamic path can be accessed. !!!
//
//       When using this snippet, the SVG only needs to reference the sprite name, no path required.
//       <svg class="header__svg-search">
//         <use xlink:href="#sprite-search"></use>
//       </svg>
//
// (function () {
//     const xhr = new XMLHttpRequest();
//     const url = './spritemap.svg';
//     xhr.open('GET', url);
//     xhr.onload = () => {
//         const div = document.createElement('div');
//         div.style.display = 'none';
//         div.innerHTML = xhr.responseText;
//         document.body.appendChild(div);
//     };
//     xhr.send();
// }());

window.PROJECT = {
  exampleComponent: new ExampleComponent()
};
