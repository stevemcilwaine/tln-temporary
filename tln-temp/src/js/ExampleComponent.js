/**
* Example component.
*
* @param {event} event - Prevents the default event of the trigger.
*/
function exampleFunction(event) {
  event.preventDefault();

}

/**
* Example component.
*
*/
export class ExampleComponent {
/**
* Constructor.
*
*/
  constructor() {

  }
}
