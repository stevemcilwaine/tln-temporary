// define routes and snapshot file names
// NOTE - chrome can have an issue with `localhost`, please use 127.0.0.1 instead
const routes = [{
  url: 'http://127.0.0.1:3003',
  name: 'home-snapshot',
  widths: [320, 768, 1024, 1400]
}];

module.exports = routes;
