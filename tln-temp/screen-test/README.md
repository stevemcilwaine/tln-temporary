# Screen Comparison Testing
=========================

## Generate browser grabs and compare / highlight differences
Ensure the app is being served locally on 3003 ( the default port ).

Add any required routes to the routes.js file in the screen-test folder, along with an associated image file name.
Please generate images for any new routes and commit these to git, use the following command to generate references images.
>`npm run generate`

### First Time

First time, you'll need to install the grabber's dependencies, simply go into the screen-test folder and install
>`cd screen-test`
>`npm install`

To generate an initial set of comparison images
>`npm run generate`

Remember to commit the ref- images in screen-test/browser-test-images to git, for others to use in comparisons.

### Subsequent Times

To run a comparison, from the screen-test folder, simply run
>`npm run compare`

This will generate grab and diff images in the screen-test/browser-test-images folder, differences are highlighted in purple, the tolerance is quite low, so there can be highlighted changes when nothing has really changed.

Compare the ref image against the grab to confirm there are no major issues.
If there is an expected change, please update the ref image with your grab and commit.
