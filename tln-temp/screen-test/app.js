const fs = require('fs');

const {Builder} = require('selenium-webdriver');
const compareImages = require('resemblejs/compareImages');
const firefox = require('selenium-webdriver/firefox');
const chrome = require('selenium-webdriver/chrome');

const routes = require('./routes.js')

/**
 * Compare a grab against a reference image and output the differences as a new image
 * @param {string} refFileName - references image input filename
 * @param {string} grabFileName - most recent grab input filename
 * @param {string} diffFileName - differences output filename
 */
async function getDiff(refFileName, grabFileName, diffFileName) {
  const options = {
    output: {
      errorColor: {
        red: 255,
        green: 0,
        blue: 255
      },
      errorType: 'movement',
      transparency: 0.3,
      largeImageThreshold: 1200,
      useCrossOrigin: false,
      outputDiff: true
    },
    scaleToSameSize: true,
    ignore: ['nothing', 'less', 'antialiasing', 'colors', 'alpha']
  };

  // load reference image and grab, and compare
  const refImage = fs.readFileSync(refFileName);
  const grabImage = fs.readFileSync(grabFileName);
  const data = await compareImages(
    refImage,
    grabImage,
    options
  );

  // write differences map to file
  console.log(`\tWriting diff ... ${diffFileName}`);
  fs.writeFileSync(diffFileName, data.getBuffer());
}

/**
 * Generate grab and comparison images for a given URL.
 * @param {string} browser - browser to use to run test ( 'firefox', 'chrome', 'safari' ).
 * @param {string} url - url to take snapshot of.
 * @param {string} name - base name for images.
 * @param {boolean?} generateOnly - flag if generate grab only, no comparison, for generating reference images.
 * @param {number?} width - browser width when generating snapshots, defaults to 1280 when omitted
 */
async function compare(browser, url, name, generateOnly, width) {
  // size
  width = width || 1280;
  const height = 6000;

  // build filenames
  const ref = `./browser-test-images/ref-${name}-${width}.png`;
  const grab = `./browser-test-images/grab-${name}-${width}.png`;
  const diff = `./browser-test-images/diff-${name}-${width}.png`;

  // options
  const firefoxOptions = new firefox.Options()
    .headless()
    .windowSize({width, height});
  const chromeOptions = new chrome.Options()
    .headless()
    .windowSize({width, height});

  // open browser
  let driver = await new Builder()
    .forBrowser(browser)
    .setFirefoxOptions(firefoxOptions)
    .setChromeOptions(chromeOptions)
    .build();
  try {
    // load page
    await driver.get(url);
    // snapshot rendered page
    const image = await driver.takeScreenshot();
    console.log(generateOnly ? `\tWriting ref ... ${ref}` : `\tWriting grab ... ${grab}`);
    fs.writeFileSync(generateOnly ? ref : grab, image, 'base64');
    // generate differences, if required
    if (!generateOnly) {
      getDiff(ref, grab, diff);
    }
  } finally {
    await driver.quit();
  }
}

/**
 * Snapshot urls and generate images.
 * @param {string} browser - name of browser to run tests in.
 * @param {any[]} links - array of links to snapshot and compare.
 * @param {boolean?} generateOnly - flag if generate grab only, no comparison, for generating reference images.
 */
async function run(browser, links, generateOnly) {
  for (const link of links) {
    if (!link.widths) {
      await compare(browser, link.url, link.name, generateOnly);
    } else {
      for (const width of link.widths) {
        await compare(browser, link.url, link.name, generateOnly, width);
      }
    }
  }
}

// run each route
const generate = -1 !== process.argv.indexOf('--generate');
run('chrome', routes, generate);
