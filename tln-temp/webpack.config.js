/* eslint-disable */
const fs = require('fs');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const cssnano = require('cssnano');
const globImporter = require('node-sass-glob-importer');
const JsDocPlugin = require('jsdoc-webpack4-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const postcssPresetEnv = require('postcss-preset-env');
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const webpack = require('webpack');
const WebpackShellPlugin = require('webpack-shell-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

module.exports = (env, argv) => {
  const buildMode = argv.mode || 'development';

  const generateVersionedCSS = () => {
    const dateString = new Date().toISOString().replace(/[-:.TZ]/g, '');

    let file = fs.readFileSync(path.join('.', 'style.css.template'));
    file = file.toString().replace(/<\$VERSION\$>/g, dateString);
    fs.writeFileSync(path.join('..', 'style.css'), file);
  };

  if (buildMode === 'production') {
    generateVersionedCSS();
  }

  const config = {
    mode: 'development',
    devServer: {
      contentBase: path.join(__dirname, './dist'),
      port: 3003,
      host: '0.0.0.0'
    },
    devtool: 'source-map',
    entry: ['./src/js/app.js', './src/scss/styles.scss'],
    output: {
      path: path.resolve(__dirname, 'dist/'),
      filename: '[name].bundle.js',
    },
    optimization: {
      minimize: true,
      minimizer: [new TerserPlugin({
        extractComments: false
      })],
    },
    module: {
      rules: [

        {
          test: /\.js$/,
          exclude: [
            path.resolve(__dirname, 'node_modules')
          ],
          use: [{
            loader: 'babel-loader',
            query: {
              presets: [
                ['@babel/preset-env', {useBuiltIns: 'usage', corejs: 3, debug: buildMode === 'development'}],
              ]
            }
          }]
        },

        {
          test: /\.s[c|a]ss$/,
          use: [
            // swap the following two lines if you need to live reload CSS changes - however this will not build CSS
            // 'development' === buildMode ? 'style-loader' : MiniCssExtractPlugin.loader,
            MiniCssExtractPlugin.loader,

            //resolve-url-loader may be chained before sass-loader if necessary
            'css-loader',  // translates CSS into CommonJS
            {
              loader: 'postcss-loader',
              options: {
                plugins: [
                  postcssPresetEnv(),
                  buildMode === 'production' ? cssnano() : false,
                ]
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sassOptions: {
                  importer: globImporter()
                }
              }
            }
          ]
        },

        {
          test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: 'url-loader?limit=10000&mimetype=application/font-woff',
          options: {
            outputPath: './fonts'
          }
        },

        {
          test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: 'file-loader',
          options: {
            outputPath: './fonts'
          }
        },

        {
          test: /\.(jpe?g|png|gif|svg)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                outputPath: './images'
              }
            },
            {
              loader: 'image-webpack-loader',
              options: {
                mozjpeg: {
                  enabled: buildMode === 'production',
                  progressive: true,
                  quality: 65
                },
                optipng: {
                  enabled: buildMode === 'production',
                },
                pngquant: {
                  enabled: buildMode === 'production',
                  quality: [0.65, 0.90],
                  speed: 4
                },
                gifsicle: {
                  enabled: buildMode === 'production',
                  interlaced: false
                },
                webp: {
                  enabled: buildMode === 'production',
                  quality: 75
                }
              }
            }
          ]
        }
      ]
    },
    plugins: [
      new CleanWebpackPlugin(),
      new webpack.ProvidePlugin({
        'window.jQuery': 'jquery',
        '$': 'jquery',
        'jQuery': 'jquery'
      }),
      new MiniCssExtractPlugin({
        filename: 'styles.css'
      }),
      new CopyWebpackPlugin([{
        from: 'assets/**/*',
        transformPath(targetPath, absolutePath) {
          return targetPath.substr('assets/'.length);
        }
      }], {
        debug: 'info'
      }),
      new SVGSpritemapPlugin('./icons/*.svg', {styles: '~sprites.scss'}),
      new WebpackShellPlugin({
        onBuildStart: ['npm run lint-style'],
        dev: false
      }),
      new JsDocPlugin({
        conf: './jsdoc.conf'
      })
    ],
    resolve: {
      extensions: ['.js', '.scss', '.gif', '.png', '.jpg', '.jpeg', '.svg']
    }
  };

  return config;
};
